import express from 'express';
import {
  login,
  checkAuth,
  getUserProfile,
  updateUserProfile,
  createUser,
  getUsers,
  updateUser,
  getUser,
  deleteUser,
} from '../controllers/user.controller.js';

const router = express.Router();

router.post('/login', login);
router.get('/auth', checkAuth);
router.route('/profile').get(getUserProfile).put(updateUserProfile);
router.route('/').post(createUser).get(getUsers);
router.route('/:id').get(getUser).delete(deleteUser).put(updateUser);

export default router;
