import fs from 'fs';
import http from 'http';
import https from 'https';
import path from 'path';
import express from 'express';
import dotenv from 'dotenv';
import colors from 'colors';
import morgan from 'morgan';
import connectDB from './config/db.js';
import { notFound, errorHandler } from './middlewares/error.middlewares.js';
import userRoutes from './routes/user.routes.js';
import courseRoutes from './routes/course.routes.js';

dotenv.config();

connectDB();

const app = express();
app.use(express.json()); // Body parser

// Certificate;
const privateKey = fs.readFileSync(
  '/etc/letsencrypt/live/skillopia.com/privkey.pem',
  'utf8'
);
const certificate = fs.readFileSync(
  '/etc/letsencrypt/live/skillopia.com/cert.pem',
  'utf8'
);
const ca = fs.readFileSync(
  '/etc/letsencrypt/live/skillopia.com/fullchain.pem',
  'utf8'
);

const credentials = {
  key: privateKey,
  cert: certificate,
  ca: ca,
};

// Morgan logger setup
const __dirname = path.resolve();
const accessLogStream = fs.createWriteStream(
  path.join(__dirname, 'access.log'),
  { flags: 'a' }
);
app.use(morgan('dev'));
app.use(morgan('combined', { stream: accessLogStream })); // Log all requests to access.log

// CORS setup
app.use(function (_req, res, next) {
  res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

// Routes
app.use('/api/v1/users', userRoutes);
app.use('/api/v1/courses', courseRoutes);

// Create a static folder
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));

if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, '/client/dist')));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'dist', 'index.html'));
  });
} else {
  app.get('/', (req, res) => {
    res.send('API is running');
  });
}

// Error middlewares
app.use(notFound);
app.use(errorHandler);

const HTTPS_PORT = process.env.HTTPS_PORT || 9001;
const HTTP_PORT = process.env.HTTP_PORT || 9002;

// Starting both http & https servers
const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

httpServer.listen(HTTP_PORT, () => {
  console.log(
    `HTTP Server running in ${process.env.NODE_ENV} mode on port ${HTTP_PORT}`
      .yellow.bold
  );
});

httpsServer.listen(HTTPS_PORT, () => {
  console.log(
    `HTTPS Server running in ${process.env.NODE_ENV} mode on port ${HTTPS_PORT}`
      .yellow.bold
  );
});

// app.listen(PORT, () => {
//   console.log(
//     `Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold
//   );
// });
