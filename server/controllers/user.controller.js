import asyncHandler from 'express-async-handler';
import User from '../models/user.model.js';
import generateToken from '../utils/generateToken.js';

// @desc    Login
// @route   POST /api/v1/users/login
// @access  public
export const login = asyncHandler(async (req, res) => {
  const { email, password } = req.body;

  const user = await User.findOne({ email });

  if (user && (await user.matchPassword(password))) {
    res.json({
      _id: user._id,
      name: user.name,
      email: user.email,
      phone: user.phone,
      token: generateToken(user._id, user.role),
    });
  } else {
    res.status(401);
    throw new Error('Invalid email or password');
  }
});

// @desc    Check token
// @route   GET /api/v1/users/auth
// @access  public
export const checkAuth = asyncHandler(async (_req, res) => {
  res.status(200).json({ message: 'Auth Success' });
});

// @desc    Get user profile
// @route   GET /api/v1/users/profile
// @access  private
export const getUserProfile = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id);

  if (user) {
    res.json({
      _id: user._id,
      name: user.name,
      email: user.email,
      phone: user.phone,
    });
  } else {
    res.status(404);
    throw new Error('User not found');
  }
});

// @desc    Update user profile
// @route   PUT /api/v1/users/profile
// @access  private
export const updateUserProfile = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id);

  if (user) {
    user.name = req.body.name || user.name;
    user.email = req.body.email || user.email;
    user.phone = req.body.phone || user.phone;
    if (req.body.password) {
      user.password = req.body.password;
    }

    const updatedUser = await user.save();

    res.json({
      _id: updatedUser._id,
      name: updatedUser.name,
      email: updatedUser.email,
      phone: updatedUser.phone,
    });
  } else {
    res.status(404);
    throw new Error('User not found');
  }
});

// @desc    Create new user
// @route   POST /api/v1/users
// @access  private/admin
export const createUser = asyncHandler(async (req, res) => {
  const { name, email, password, phone } = req.body;

  const userExists = await User.findOne({ email });

  if (userExists) {
    res.status(400);
    throw new Error('User already exists');
  }

  const user = await User.create({
    name,
    email,
    password,
    phone,
  });

  if (user) {
    res.status(201).json({
      _id: user._id,
      name: user.name,
      email: user.email,
      phone: user.phone,
    });
  } else {
    res.status(400);
    throw new Error('Invalid data');
  }
});

// @desc    Get all users
// @route   GET /api/v1/users?page=1&size=10
// @access  private/admin
export const getUsers = asyncHandler(async (req, res) => {
  try {
    let { page, size } = req.query;

    if (!page) page = 1;
    if (!size) size = 10;

    const limit = parseInt(size);
    const skip = (+page - 1) * size;

    const users = await User.find().limit(limit).skip(skip).select('-password');

    const totalUsersCount = await User.count('_id');

    res.json({
      total: totalUsersCount,
      page,
      size,
      count: users.length,
      users,
    });
  } catch (error) {
    res.status(500).json({ message: 'Invalid query' });
  }
});

// @desc    Update user
// @route   PUT /api/v1/users/:id
// @access  private/admin
export const updateUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id);

  if (user) {
    user.name = req.body.name || user.name;
    user.email = req.body.email || user.email;
    user.phone = req.body.phone || user.phone;
    if (req.body.password) {
      user.password = req.body.password;
    }

    const updatedUser = await user.save();

    res.json({
      _id: updatedUser._id,
      name: updatedUser.name,
      email: updatedUser.email,
      phone: updatedUser.phone,
    });
  } else {
    res.status(404);
    throw new Error('User not found');
  }
});

// @desc    Get single user
// @route   GET /api/v1/users/:id
// @access  private/admin
export const getUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id).select('-password');

  if (user) {
    res.json(user);
  } else {
    res.status(404).json({ message: 'User not found' });
  }
});

// @desc    Delete a user
// @route   DELETE /api/v1/users/:id
// @access  private/admin
export const deleteUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id);

  if (user) {
    await user.remove();
    res.json({ message: 'User deleted' });
  } else {
    res.status(404);
    throw new Error('User not found');
  }
});
