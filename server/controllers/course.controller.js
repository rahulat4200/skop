import asyncHandler from 'express-async-handler';
import Course from '../models/course.model.js';

// @desc    Fetch all courses
// @route   GET /api/v1/courses?page=1&size=10&category=networking
// @access  private
export const getCourses = asyncHandler(async (req, res) => {
  try {
    let { page, size, category } = req.query;

    if (!page) page = 1;
    if (!size) size = 10;

    const limit = parseInt(size);
    const skip = (+page - 1) * size;

    const courses = await Course.find({
      ...(category && { category: category }),
    })
      .limit(limit)
      .skip(skip);

    res.json({
      page,
      size,
      count: courses.length,
      courses,
      total: courses.length,
    });
  } catch (error) {
    res.status(500).json({ message: 'Invalid query' });
  }
});

// @desc    Fetch course by ID
// @route   GET /api/v1/courses/:id
// @access  private
export const getCourseById = asyncHandler(async (req, res) => {
  const course = await Course.findById(req.params.id);

  if (course) {
    res.json(course);
  } else {
    res.status(404).json({ message: 'Course not found' });
  }
});

// @desc    Delete a course
// @route   DELETE /api/v1/courses/:id
// @access  private/admin
export const deleteCourse = asyncHandler(async (req, res) => {
  const course = await Course.findById(req.params.id);

  if (course) {
    await course.remove();
    res.json({ message: 'Course deleted' });
  } else {
    res.status(404);
    throw new Error('Course not found');
  }
});

// @desc    Create a course
// @route   POST /api/v1/courses
// @access  private/admin
export const createCourse = asyncHandler(async (req, res) => {
  const {
    title,
    summary,
    category,
    price,
    duration,
    metadata,
    videoUrl,
    courseTopics,
    labTopics,
  } = req.body;

  const course = new Course({
    title,
    summary,
    category,
    price,
    duration,
    metadata,
    videoUrl,
    courseTopics,
    labTopics,
  });

  const createdCourse = await course.save();
  res.status(201).json(createdCourse);
});

// @desc    Update a course
// @route   PUT /api/v1/courses/:id
// @access  private/admin
export const updateCourse = asyncHandler(async (req, res) => {
  const {
    title,
    summary,
    category,
    price,
    duration,
    metadata,
    videoUrl,
    courseTopics,
    labTopics,
  } = req.body;

  const course = await Course.findById(req.params.id);

  if (course) {
    course.title = title || course.title;
    course.summary = summary || course.summary;
    course.category = category || course.category;
    course.price = price || course.price;
    course.duration = duration || course.duration;
    course.metadata = metadata || course.metadata;
    course.videoUrl = videoUrl || course.videoUrl;
    course.courseTopics = courseTopics || course.courseTopics;
    course.labTopics = labTopics || course.labTopics;

    const updatedCourse = await course.save();
    res.json(updatedCourse);
  } else {
    res.status(404);
    throw new Error('Course not found');
  }
});
