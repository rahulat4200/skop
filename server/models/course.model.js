import mongoose from 'mongoose';

const courseSchema = mongoose.Schema(
  {
    title: {
      type: String,
      requird: true,
    },
    summary: {
      type: String,
      requird: true,
    },
    category: {
      type: String,
      requird: true,
    },
    price: {
      type: Number,
      requird: true,
    },
    duration: {
      type: Number,
    },
    metadata: {
      pageCount: Number,
      videoCount: Number,
      labCount: Number,
    },
    videoUrl: String,
    courseTopics: [{ title: String, content: String }],
    labTopics: [String],
  },
  { timestamps: true, collection: 'courses' }
);

const Course = mongoose.model('Course', courseSchema);

export default Course;
