import { userApiService } from '../api.service';

export const courseApi = userApiService.injectEndpoints({
  endpoints: (builder) => ({
    getAllCourses: builder.query({
      query: ({ page = 1, size = 10 }) => `/courses?page=${page}&size=${size}`,
      providesTags: ['Course'],
    }),
    addCourse: builder.mutation({
      query: ({ id, ...data }) => ({
        url: '/courses',
        method: 'POST',
        body: data,
      }),
      transformResponse: (response, _meta, _arg) => response,
      invalidatesTags: ['Course'],
    }),
    deleteCourse: builder.mutation({
      query: ({ id }) => ({
        url: `/courses/${id}`,
        method: 'DELETE',
      }),
      transformResponse: (response, _meta, _arg) => response,
      invalidatesTags: ['Course'],
    }),
    getSingleCourse: builder.query({
      query: ({ id }) => `/courses/${id}`,
      providesTags: ['Course'],
    }),
    updateCourse: builder.mutation({
      query: ({ id, data }) => ({
        url: `/courses/${id}`,
        method: 'PUT',
        body: data,
      }),
      transformResponse: (response, _meta, _arg) => response,
      invalidatesTags: ['Course'],
    }),
  }),
});

export const {
  useGetAllCoursesQuery,
  useGetSingleCourseQuery,
  useAddCourseMutation,
  useUpdateCourseMutation,
  useDeleteCourseMutation,
} = courseApi;
