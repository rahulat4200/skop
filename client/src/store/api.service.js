import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const API_URL = '/api/v1/';

export const userApiService = createApi({
  reducerPath: 'userAPI',
  baseQuery: fetchBaseQuery({
    baseUrl: `${API_URL}`,
  }),
  endpoints: () => ({}),
});
