import { BrowserRouter, Routes, Route } from 'react-router-dom';

// Views
import Home from './views/landing/Home/Home';
import Login from './views/landing/Login/Login';
import SingleCourse from './views/landing/SingleCourse/SingleCourse';
import Support from './views/landing/Support/Support';

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/login' element={<Login />} />
        <Route path='/courses/:id' element={<SingleCourse />} />
        <Route path='/support' element={<Support />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
