import Footer from '@/components/layout/navigation/Footer/Footer';
import Navbar from '@/components/layout/navigation/Navbar/Navbar';
import Spacer from '@/components/layout/ui/Spacer/Spacer';
import Banner from '@/components/pages/home/Banner/Banner';
import Categories from '@/components/pages/home/Categories/Categories';
import CTA from '@/components/pages/home/CTA/CTA';
import FeatureArea from '@/components/pages/home/FeatureArea/FeatureArea';
import FeaturesCourses from '@/components/pages/home/FeaturedCourses/FeaturesCourses';
import Hero from '@/components/pages/home/Hero/Hero';
import PopularCourses from '@/components/pages/home/PopularCourses/PopularCourses';

const Home = () => {
  return (
    <>
      <Spacer />
      <Navbar />
      <Hero />
      <Banner />
      <PopularCourses />
      <Categories />
      <FeatureArea />
      <FeaturesCourses />
      <CTA />
      <Footer />
    </>
  );
};

export default Home;
