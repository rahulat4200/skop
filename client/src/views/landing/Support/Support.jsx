import Spacer from '@/components/layout/ui/Spacer/Spacer';
import Navbar from '@/components/layout/navigation/Navbar/Navbar';

const Support = () => {
  return (
    <>
      <Spacer />
      <Navbar />

      <div className='relative pt-20'>
        <div className='max-w-7xl mx-auto sm:px-6 lg:px-8'>
          <div className='relative shadow-xl sm:rounded-2xl sm:overflow-hidden'>
            <div className='absolute inset-0'>
              <img
                className='h-full w-full object-cover'
                src='https://images.unsplash.com/photo-1521737852567-6949f3f9f2b5?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=2830&q=80&sat=-100'
                alt='People working on laptops'
              />
              <div className='absolute inset-0 bg-indigo-700 mix-blend-multiply' />
            </div>
            <div className='relative px-4 py-16 sm:px-6 sm:py-24 lg:py-32 lg:px-8'>
              <h1 className='text-center text-4xl font-extrabold tracking-tight sm:text-5xl lg:text-6xl'>
                <span className='block text-white'>Level up your career</span>
                <span className='block text-indigo-200'>Get in touch</span>
              </h1>
            </div>
          </div>
        </div>
      </div>

      <div class='bg-white'>
        <div class='max-w-7xl mx-auto py-16 px-4 sm:px-6 lg:py-24 lg:px-8'>
          <div class='divide-y-2 divide-gray-200'>
            <div class='lg:grid lg:grid-cols-3 lg:gap-8'>
              <h2 class='text-2xl font-extrabold text-gray-900 sm:text-3xl'>
                Contact Details
              </h2>
              <div class='mt-8 grid grid-cols-1 gap-12 sm:grid-cols-2 sm:gap-x-8 sm:gap-y-12 lg:mt-0 lg:col-span-2'>
                <div>
                  <h3 class='text-lg leading-6 font-medium text-gray-900'>
                    Careers
                  </h3>
                  <dl class='mt-2 text-base text-gray-500'>
                    <div>
                      <dt class='sr-only'>Email</dt>
                      <dd>careers@skillopia.com</dd>
                    </div>
                    <div class='mt-1'>
                      <dt class='sr-only'>Phone number</dt>
                      <dd>+91 09820611290</dd>
                    </div>
                  </dl>
                </div>
                <div>
                  <h3 class='text-lg leading-6 font-medium text-gray-900'>
                    Support
                  </h3>
                  <dl class='mt-2 text-base text-gray-500'>
                    <div>
                      <dt class='sr-only'>Email</dt>
                      <dd>support@skillopia.com</dd>
                    </div>
                    <div class='mt-1'>
                      <dt class='sr-only'>Phone number</dt>
                      <dd>+91 09820611290</dd>
                    </div>
                  </dl>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Support;
