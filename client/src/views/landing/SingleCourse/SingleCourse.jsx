import { useParams } from 'react-router-dom';
import { useGetSingleCourseQuery } from '@/store/courses/courses.slice';
import Spacer from '@/components/layout/ui/Spacer/Spacer';
import Navbar from '@/components/layout/navigation/Navbar/Navbar';
import Footer from '@/components/layout/navigation/Footer/Footer';
import CourseHeader from '@/components/pages/single-course/CourseHeader/CourseHeader';
import TopicsAccordion from '@/components/pages/single-course/TopicsAccordion/TopicsAccordion';
import LabsTopicAccordion from '@/components/pages/single-course/LabTopicsAccordion/LabTopicsAccordion';
import CourseCTA from '@/components/pages/single-course/CourseCTA/CourseCTA';

const SingleCourse = () => {
  const { id } = useParams();

  const { data, error, isLoading, isFetching, isSuccess } =
    useGetSingleCourseQuery({ id });

  return (
    <>
      <Spacer />
      {isLoading && <p>Loading...</p>}
      {!error && !isLoading && (
        <>
          <CourseHeader
            title={data.title}
            summary={data.summary}
            category={data.category}
            labCount={data.metadata.labCount}
            pageCount={data.metadata.pageCount}
            videoCount={data.metadata.videoCount}
            duration={data.duration}
            price={data.price}
            videoUrl={data.videoUrl}
          />
          <div className='relative bg-white pt-12 pb-16 border-t px-4 sm:px-6 lg:pt-16 lg:pb-28 lg:px-8'>
            <div className='max-w-7xl mx-auto'>
              <div className='grid grid-cols-12 gap-16'>
                <div className='col-span-7'>
                  {data.courseTopics?.length > 0 ? (
                    <TopicsAccordion topics={data.courseTopics} />
                  ) : (
                    <p>No topics found</p>
                  )}
                </div>
                <div className='col-span-5'>
                  {data.labTopics?.length > 0 ? (
                    <LabsTopicAccordion topics={data.labTopics} />
                  ) : (
                    <p>No labs found</p>
                  )}
                </div>
              </div>
            </div>
          </div>
          <CourseCTA title={data.title} />
        </>
      )}
      <Navbar />
      <Footer />
    </>
  );
};

export default SingleCourse;
