import { Disclosure } from '@headlessui/react';

const LabsTopicsAccordion = ({ topics }) => {
  return (
    <div className='bg-white'>
      <div className='max-w-7xl mx-auto'>
        <h3 className='leading-6 text-indigo-600 font-semibold tracking-wide uppercase'>
          Labs List
        </h3>
        <div className='divide-y-2 divide-gray-200 max-h-96 overflow-auto'>
          <dl className='mt-6 space-y-4 divide-y divide-gray-200'>
            {topics.map((topic, i) => (
              <Disclosure as='div' key={i} className='pt-4'>
                {({ open }) => (
                  <>
                    <dt className='text-lg'>
                      <Disclosure.Button className='text-left w-full flex justify-between items-start text-gray-400'>
                        <span className='font-medium text-gray-900'>
                          {topic}
                        </span>
                      </Disclosure.Button>
                    </dt>
                  </>
                )}
              </Disclosure>
            ))}
          </dl>
        </div>
      </div>
    </div>
  );
};

export default LabsTopicsAccordion;
