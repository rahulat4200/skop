import { Disclosure } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/outline';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

const TopicsAccordion = ({ topics }) => {
  return (
    <div className='bg-white'>
      <div className='max-w-7xl mx-auto'>
        <h3 className='leading-6 text-indigo-600 font-semibold tracking-wide uppercase'>
          Course Topics
        </h3>
        <div className='divide-y-2 divide-gray-200 max-h-96 overflow-auto'>
          <dl className='mt-6 space-y-4 divide-y divide-gray-200'>
            {topics.map((topic) => (
              <Disclosure as='div' key={topic.question} className='pt-4'>
                {({ open }) => (
                  <>
                    <dt className='text-lg'>
                      <Disclosure.Button className='text-left w-full flex justify-between items-start text-gray-400'>
                        <span className='font-medium text-gray-900'>
                          {topic.title}
                        </span>
                        <span className='ml-6 h-7 flex items-center'>
                          <ChevronDownIcon
                            className={classNames(
                              open ? '-rotate-180' : 'rotate-0',
                              'h-6 w-6 transform'
                            )}
                            aria-hidden='true'
                          />
                        </span>
                      </Disclosure.Button>
                    </dt>
                    <Disclosure.Panel as='dd' className='mt-2 pr-12'>
                      <div
                        dangerouslySetInnerHTML={{ __html: topic.content }}
                        className='course-topics'></div>
                    </Disclosure.Panel>
                  </>
                )}
              </Disclosure>
            ))}
          </dl>
        </div>
      </div>
    </div>
  );
};

export default TopicsAccordion;
