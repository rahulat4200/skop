import {
  TerminalIcon,
  DocumentDuplicateIcon,
  VideoCameraIcon,
  ClockIcon,
} from '@heroicons/react/outline';
import { Link } from 'react-router-dom';
import GoBackLink from './GoBackLink';

const CourseHeader = ({
  title,
  summary,
  category,
  videoUrl,
  labCount,
  pageCount,
  videoCount,
  duration,
  price,
}) => {
  return (
    <>
      <GoBackLink />
      <div className='relative bg-white pt-12 pb-16 px-4 sm:px-6 lg:pt-16 lg:pb-20 lg:px-8'>
        <div className='max-w-7xl mx-auto'>
          {/* Header */}
          <h6 className='text-sm text-indigo-700 font-semibold mb-5 bg-indigo-50 inline-block px-4 py-1 rounded-full'>
            {category}
          </h6>
          <div className='grid grid-cols-12 gap-16'>
            {/* Category, Title and Description */}
            <div className='col-span-7'>
              <h1 className='font-bold text-3xl text-gray-900 mb-4'>{title}</h1>
              <p className='text-gray-600'>{summary}</p>
              {/* Icons */}
              <div className='flex gap-12 mt-10'>
                <div className='flex items-center flex-col gap-1'>
                  <span className='text-md text-gray-400 block'>
                    <TerminalIcon
                      className='flex-shrink-0 h-7 w-7 text-indigo-400'
                      aria-hidden='true'
                    />
                  </span>
                  <span className='text-sm text-gray-800 font-semibold block'>
                    {labCount} Labs
                  </span>
                </div>
                <div className='flex items-center flex-col gap-1'>
                  <span className='text-md text-gray-400 block'>
                    <DocumentDuplicateIcon
                      className='flex-shrink-0 h-7 w-7 text-indigo-400'
                      aria-hidden='true'
                    />
                  </span>
                  <span className='text-sm text-gray-800 font-semibold block'>
                    {pageCount} Labs
                  </span>
                </div>
                <div className='flex items-center flex-col gap-1'>
                  <span className='text-md text-gray-400 block'>
                    <VideoCameraIcon
                      className='flex-shrink-0 h-7 w-7 text-indigo-400'
                      aria-hidden='true'
                    />
                  </span>
                  <span className='text-sm text-gray-800 font-semibold block'>
                    {videoCount} Videos
                  </span>
                </div>
                <div className='flex items-center flex-col gap-1'>
                  <span className='text-md text-gray-400 block'>
                    <ClockIcon
                      className='flex-shrink-0 h-7 w-7 text-indigo-400'
                      aria-hidden='true'
                    />
                  </span>
                  <span className='text-sm text-gray-800 font-semibold block'>
                    {duration} Hours
                  </span>
                </div>
              </div>
              {/* Price and Enroll Button */}
              <div className='flex items-center mt-12 gap-10'>
                <Link
                  to='/'
                  className='px-8 py-3 text-base bg-gray-900 text-white font-bold inline-block rounded-md uppercase hover:bg-indigo-900 transition-all hover:shadow-lg focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'>
                  Enroll Now
                </Link>
                <span className='block text-3xl font-bold text-indigo-800'>
                  {price}{' '}
                  <span className='text-lg text-indigo-500 -ml-0.5'>
                    Tokens
                  </span>
                </span>
              </div>
            </div>
            {/* Video */}
            <div className='col-span-5'>
              {videoUrl && (
                <iframe
                  class='w-full aspect-video rounded-lg'
                  src={`${videoUrl}?controls='0'`}
                  title='Skillopia'
                  frameborder='0'
                  allow='accelerometer; autoplay; modestbranding; clipboard-write; encrypted-media; gyroscope;'
                  allowfullscreen></iframe>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CourseHeader;
