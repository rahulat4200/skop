import { useNavigate } from 'react-router-dom';
import { ArrowLeftIcon } from '@heroicons/react/outline';

const GoBackLink = () => {
  const navigate = useNavigate();

  return (
    <div className='relative bg-gradient-to-b from-gray-100 to-white py-3 px-4 lg:px-8'>
      <div className='max-w-7xl mx-auto'>
        <button
          onClick={() => navigate(-1)}
          className='text-sm font-medium text-gray-600 relative top-0.5 hover:text-gray-900'>
          <div className='flex gap-2 items-center'>
            <ArrowLeftIcon className='text-sm text-gray-600 h-4 w-4' />
            Go Back
          </div>
        </button>
      </div>
    </div>
  );
};

export default GoBackLink;
