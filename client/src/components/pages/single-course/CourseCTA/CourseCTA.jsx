const CourseCTA = ({ title }) => {
  return (
    <div className='bg-indigo-700'>
      <div className='max-w-5xl mx-auto text-center py-16 px-4 sm:py-20 sm:px-6 lg:px-8'>
        <h2 className='text-3xl font-extrabold text-white sm:text-4xl'>
          <span className='block'>Enroll now and become a master.</span>
          <span className='block'>Start your upskilling journey.</span>
        </h2>
        <a
          href='#'
          className='mt-8 w-full inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-indigo-600 bg-white hover:bg-indigo-50 sm:w-auto'>
          Enroll Now
        </a>
      </div>
    </div>
  );
};

export default CourseCTA;
