import CourseCard from '@/components/layout/ui/CourseCard/CourseCard';
import SectionTitle from '@/components/layout/ui/SectionTitle/SectionTitle';
import { useGetAllCoursesQuery } from '@/store/courses/courses.slice';

const PopularCourses = () => {
  const { data, error, isLoading, isFetching, isSuccess } =
    useGetAllCoursesQuery({});

  return (
    <div className='relative bg-gray-50 pt-12 pb-16 px-4 sm:px-6 lg:pt-24 lg:pb-28 lg:px-8'>
      <div className='max-w-7xl mx-auto'>
        <SectionTitle
          title='Popular Courses'
          content='The best courses we have'
        />

        <div className='mt-12 max-w-lg mx-auto grid gap-4 lg:grid-cols-4 lg:max-w-none'>
          {isLoading ? (
            <p>laoding</p>
          ) : (
            data &&
            data?.courses
              ?.slice(0, 8)
              .map((course) => (
                <CourseCard
                  name={course.title}
                  category={course.category}
                  labCount={course.metadata?.labCount}
                  pageCount={course.metadata?.labCount}
                  videoCount={course.metadata?.labCount}
                  url={`/courses/${course._id}`}
                  price={course.price}
                />
              ))
          )}
        </div>
      </div>
    </div>
  );
};

export default PopularCourses;
