import { Link } from 'react-router-dom';
import SectionTitle from '@/components/layout/ui/SectionTitle/SectionTitle';
import {
  BsHddNetwork,
  BsCodeSlash,
  BsBug,
  BsCloud,
  BsBank,
  BsMegaphone,
  BsBarChartLine,
  BsPalette2,
  BsImage,
  BsPeople,
} from 'react-icons/bs';

const Categories = () => {
  return (
    <div className='relative bg-gray-50 pb-16 px-4 sm:px-6 lg:pb-32 lg:py-20 lg:pt-10 lg:px-8'>
      <div className='max-w-7xl mx-auto'>
        <SectionTitle
          title='Course Categories'
          content='The most popular categories'
        />

        <div className='mt-12 max-w-lg mx-auto grid gap-4 lg:grid-cols-5 lg:max-w-none'>
          {/* IT & Networking */}
          <Link
            to='/'
            className='bg-white shadow p-5 rounded-lg flex items-center flex-col justify-center transition-all hover:scale-110'>
            <BsHddNetwork className='text-3xl text-indigo-600' />
            <span className='font-semibold mt-2'>IT &amp; Networking</span>
          </Link>
          {/* Software Development */}
          <Link
            to='/'
            className='bg-white shadow p-5 rounded-lg flex items-center flex-col justify-center transition-all hover:scale-110'>
            <BsCodeSlash className='text-3xl text-indigo-600' />
            <span className='font-semibold mt-2'>Software Development</span>
          </Link>
          {/* Cyber Security */}
          <Link
            to='/'
            className='bg-white shadow p-5 rounded-lg flex items-center flex-col justify-center transition-all hover:scale-110'>
            <BsBug className='text-3xl text-indigo-600' />
            <span className='font-semibold mt-2'>Cyber Security</span>
          </Link>
          {/* Cloud */}
          <Link
            to='/'
            className='bg-white shadow p-5 rounded-lg flex items-center flex-col justify-center transition-all hover:scale-110'>
            <BsCloud className='text-3xl text-indigo-600' />
            <span className='font-semibold mt-2'>Cloud</span>
          </Link>
          {/* Banking &amp; Finance */}
          <Link
            to='/'
            className='bg-white shadow p-5 rounded-lg flex items-center flex-col justify-center transition-all hover:scale-110'>
            <BsBank className='text-3xl text-indigo-600' />
            <span className='font-semibold mt-2'>Banking &amp; Finance</span>
          </Link>
          {/* Advertising & Marketing */}
          <Link
            to='/'
            className='bg-white shadow p-5 rounded-lg flex items-center flex-col justify-center transition-all hover:scale-110'>
            <BsMegaphone className='text-3xl text-indigo-600' />
            <span className='font-semibold mt-2'>Ads &amp; Marketing</span>
          </Link>
          {/* Sales */}
          <Link
            to='/'
            className='bg-white shadow p-5 rounded-lg flex items-center flex-col justify-center transition-all hover:scale-110'>
            <BsBarChartLine className='text-3xl text-indigo-600' />
            <span className='font-semibold mt-2'>Sales</span>
          </Link>
          {/* Design */}
          <Link
            to='/'
            className='bg-white shadow p-5 rounded-lg flex items-center flex-col justify-center transition-all hover:scale-110'>
            <BsPalette2 className='text-3xl text-indigo-600' />
            <span className='font-semibold mt-2'>Design</span>
          </Link>
          {/* Photography */}
          <Link
            to='/'
            className='bg-white shadow p-5 rounded-lg flex items-center flex-col justify-center transition-all hover:scale-110'>
            <BsImage className='text-3xl text-indigo-600' />
            <span className='font-semibold mt-2'>Photography</span>
          </Link>
          {/* Human Resources */}
          <Link
            to='/'
            className='bg-white shadow p-5 rounded-lg flex items-center flex-col justify-center transition-all hover:scale-110'>
            <BsPeople className='text-3xl text-indigo-600' />
            <span className='font-semibold mt-2'>Human Resources</span>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Categories;
