import {
  TerminalIcon,
  DocumentDuplicateIcon,
  VideoCameraIcon,
} from '@heroicons/react/outline';
import { Link } from 'react-router-dom';

const CourseCard = ({
  name,
  category,
  labCount,
  pageCount,
  videoCount,
  url,
  price,
}) => {
  return (
    <article className='bg-white shadow sm:rounded-lg hover:-translate-y-2 transition-all'>
      {/* Category & Name */}
      <div className='px-4 pt-6 pb-5 sm:px-6' style={{ minHeight: '120px' }}>
        <span className='text-sm font-medium text-indigo-700 block mb-2'>
          {category}
        </span>
        <h3 className='text-xl leading-6 font-semibold text-gray-900'>
          {name}
        </h3>
      </div>
      {/* Icons */}
      <div className='px-4 pt-4 pb-3 sm:px-2 border-t'>
        <div className='grid gap-2 grid-cols-3'>
          {/* Labs */}
          <div className='flex items-center flex-col gap-1'>
            <span className='text-sm text-gray-400 block'>
              <TerminalIcon
                className='flex-shrink-0 h-6 w-6 text-indigo-400'
                aria-hidden='true'
              />
            </span>
            <span className='text-xs text-gray-800 font-semibold block'>
              {labCount} Labs
            </span>
          </div>
          {/* Pages */}
          <div className='flex items-center flex-col gap-1'>
            <span className='text-sm text-gray-400 block'>
              <DocumentDuplicateIcon
                className='flex-shrink-0 h-6 w-6 text-indigo-400'
                aria-hidden='true'
              />
            </span>
            <span className='text-xs text-gray-800 font-semibold block'>
              {pageCount} Pages
            </span>
          </div>
          {/* Videos */}
          <div className='flex items-center flex-col gap-1'>
            <span className='text-sm text-gray-400 block'>
              <VideoCameraIcon
                className='flex-shrink-0 h-6 w-6 text-indigo-400'
                aria-hidden='true'
              />
            </span>
            <span className='text-xs text-gray-800 font-semibold block'>
              {videoCount} Videos
            </span>
          </div>
        </div>
      </div>
      {/* Button & Price */}
      <div className='px-4 pt-4 pb-5 sm:px-6 flex justify-between items-center'>
        <span className='block text-2xl font-bold text-indigo-800'>
          {price} <span className='text-base text-indigo-500 -ml-0.5'>TKO</span>
        </span>
        <Link
          to={url}
          className='px-4 py-1.5 border border-indigo-600 rounded-md shadow-sm text-sm font-semibold text-indigo-700 bg-white hover:bg-indigo-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 transition-all'>
          Course Details
        </Link>
      </div>
    </article>
  );
};

export default CourseCard;
