const SectionTitle = ({ title, content }) => {
  return (
    <div className='text-base text-center lg:max-w-none'>
      <h2 className='leading-6 text-indigo-600 font-semibold tracking-wide uppercase'>
        {title}
      </h2>
      <p className='mt-1 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl'>
        {content}
      </p>
    </div>
  );
};

export default SectionTitle;
