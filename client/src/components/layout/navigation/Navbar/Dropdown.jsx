import { Link } from 'react-router-dom';
import {
  CalendarIcon,
  ChartBarIcon,
  RefreshIcon,
  ShieldCheckIcon,
  ViewGridIcon,
} from '@heroicons/react/outline';

const Dropdown = () => {
  return (
    <>
      <Link
        to='/'
        className='-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50'>
        <ChartBarIcon
          className='flex-shrink-0 h-6 w-6 text-indigo-600'
          aria-hidden='true'
        />
        <div className='ml-4'>
          <p className='text-base font-medium text-gray-900'>
            IT &amp; Networking
          </p>
        </div>
      </Link>
      <Link
        to='/'
        className='-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50'>
        <CalendarIcon
          className='flex-shrink-0 h-6 w-6 text-indigo-600'
          aria-hidden='true'
        />
        <div className='ml-4'>
          <p className='text-base font-medium text-gray-900'>
            Software Development
          </p>
        </div>
      </Link>
      <Link
        to='/'
        className='-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50'>
        <ShieldCheckIcon
          className='flex-shrink-0 h-6 w-6 text-indigo-600'
          aria-hidden='true'
        />
        <div className='ml-4'>
          <p className='text-base font-medium text-gray-900'>Human Resources</p>
        </div>
      </Link>
      <Link
        to='/'
        className='-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50'>
        <ViewGridIcon
          className='flex-shrink-0 h-6 w-6 text-indigo-600'
          aria-hidden='true'
        />
        <div className='ml-4'>
          <p className='text-base font-medium text-gray-900'>
            Banking &amp; Finance
          </p>
        </div>
      </Link>
      <Link
        to='/'
        className='-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50'>
        <RefreshIcon
          className='flex-shrink-0 h-6 w-6 text-indigo-600'
          aria-hidden='true'
        />
        <div className='ml-4'>
          <p className='text-base font-medium text-gray-900'>
            Sales &amp; Marketing
          </p>
        </div>
      </Link>
    </>
  );
};

export default Dropdown;
